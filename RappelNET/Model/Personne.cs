﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RappelNET.Model
{
    class PersonneJava
    {
        private String firstname;
        private String lastname;

        public String getFirstname()
        {
            return this.firstname;
        }
    }

    class PersonneLegacy
    {
        private string _firstname;

        public string GetFirstname()
        {
            return _firstname;
        }

        public void SetFirstname(string _firstname)
        {
            _firstname = _firstname;
        }
    }


    class Personne
    {
        public Personne(string firstname, string lastname)
        {
            Firstname = firstname;
            Lastname = lastname;
        }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
